xquery version "3.1";

declare namespace mods="http://www.loc.gov/mods/v3";
declare namespace mets="http://www.loc.gov/METS/";


let $col := collection("/db/apps/digizeit/data/mets")

return
[
    for $METS in ($col/mets:mets)
    let $main := $METS//mets:dmdSec[1]//mods:mods

    let $class := string-join(($main/mods:classification ! ("d"||.)), " "),
        $lang := string($main/mods:language/mods:languageTerm),
        $lang := if( $lang = "" ) then distinct-values($METS//mods:languageTerm/text()) => string-join("; ") else $lang
    return
        array{
            $class,
            string-join($main/mods:titleInfo/mods:title, "; "),
            string($main/mods:part/mods:detail/mods:number),
            string-join($main/mods:originInfo/mods:dateIssued, "; "),
            $lang,
            string-join($main/mods:originInfo/mods:publisher[.!="DigiZeitschriften e. V."][.!="Niedersächsische Staats- und Universitätsbibliothek Göttingen"], "; "),
            string-join($main/mods:physicalDescription/mods:extent, " "),
            string($main/mods:subject/mods:topic),
            string($main/mods:recordInfo/mods:recordIdentifier)
(:        ,$METS/base-uri():)
        }
]
